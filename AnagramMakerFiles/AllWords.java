import java.util.ArrayList;
import java.util.Scanner;

/**
 *  findAllWords() searches an ArrayList dictionary database to find all words
 *	that match a string of characters.
 *
 *  Requires class OpenFile.
 *
 *  @author Mr Greenstein
 *  @version January 6, 2016
 *
 */

public class AllWords {

	private static ArrayList<String> dict = new ArrayList<String>();	// dictionary database
	
	public AllWords() {
		dict = new ArrayList<String>();
	}
	
	/**
	 *	Input dictionary words into database
	 *
	 *	@param fileName - Name of file containing the dictionary
	 */
	public static void inputDict(String fileName) {
		// Open the database file
		Scanner infile = OpenFile.openToRead(fileName);
		while (infile.hasNext()) {
			String word = infile.next();
			dict.add(word);
		}
		infile.close();
	}


	/**
	 *  Returns all the words that match a String of characters.
	 *
	 *	@param letters - a string of letters to use in anagram
	 *	@param exact - true if number of letters must match word exactly,
	 *				otherwise any number of letters may match
	 *	@return  An ArrayList of dict words found to match the letters
	 */
	public static ArrayList<String> findAllWords(String letters, boolean exact) {
	
		ArrayList<String> foundWords = new ArrayList<String>();	
							
		for (String word : dict)
			if (wordMatch(word, letters))
				if (exact && word.length() == letters.length())
					foundWords.add(word);
				else if (! exact)
					foundWords.add(word);
		
		return foundWords;
	}
	
	/**
	 *  Decides if a word matches a group of letters.
	 *
	 *  @param word  The word to test.
	 *  @param letters  A string of letters to compare
	 *  @return  true if the word matches the letters, false otherwise
	 */
	public static boolean wordMatch (String word, String letters) {
        for(int i = 0; i < word.length(); i++){
        	int ind = letters.indexOf(word.charAt(i));
            if(ind == -1)
            	return false;
            letters = letters.substring(0,ind) + letters.substring(ind+1);
        }
        return true;
	}
}