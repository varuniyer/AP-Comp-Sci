import java.util.ArrayList;
import java.util.Scanner;

/**
 *	Finds anagrams to an input string.
 *
 *	@author Varun Iyer
 *  @version 1.18.16
 */

public class AnagramMaker implements Input {

	public static int printsLeft;
	public static int totalPrints;

	public AnagramMaker() {
		// Load the dictionary
		AllWords.inputDict("wordlist.txt");
	}

	public static void main(String[] args) {
		AnagramMaker am = new AnagramMaker();
		am.run();
	}

	/**
	 *	The main routine for running AnagramMaker
	 */
	public void run() {
		printIntroduction();
		String word = parseInput(getWord());
		while(!word.equals("q"))
		{
			ArrayList<String> words = AllWords.findAllWords(word, false);
			int numOfWords = getWordsPerPhrase();
			totalPrints = printsLeft = getMax();
			System.out.println();
			findAnagrams(word, word, words, numOfWords, "");
			System.out.println();
			word = parseInput(getWord());
		}
		System.out.println("\nGoodbye!\n");
	}

	/**
	 *  Gets input word to find anagrams for
	 */
	public String getWord()
	{
		return Prompt.getString("Word(s), name, or phrase (q to quit)");
	}

	/**
	 *  Gets desired words per printed anagram
	 */
	public int getWordsPerPhrase()
	{
		return Prompt.getInt("Number of words in phrase");
	}

	/**
	 *  Gets maximum number of anagrams to print
	 */
	public int getMax()
	{
		return Prompt.getInt("Number of phrases");
	}

	/**
	 *  Takes punctuation and spaces out of inputted word
	 */
	public String parseInput(String word)
	{
		String correct = "";
		for(int i = 0; i < word.length(); i++)
		{
			if((word.charAt(i) <= 122 && word.charAt(i) >= 97) ||
			   (word.charAt(i) <= 90 && word.charAt(i) >= 65))
				correct += word.charAt(i);
		}

		return correct.toLowerCase();
	}

	/**
	 *  Find anagrams of a the given word recursively by
	 *  gathering combinations of words from findAllWords
	 *  and checking if they include all the same letters
	 *  as the inputted word and print them out if so until
	 *  either running out of words or reaching the specified
	 *  maximum anagrams to be displayed
	 */
	public void findAnagrams(String mainWord, String word, ArrayList<String> words, int anagramLen, String anagram)
	{
		if(printsLeft > 0)
		{
			if(anagramLen == 1)
			{
				words = AllWords.findAllWords(mainWord, true);
				for(int i = 0; i < words.size(); i++)
					if(i < printsLeft)
						System.out.println(words.get(i));
			}

			if(word.equals("") && !anagram.equals("") && anagram.split(" ").length == anagramLen)
			{
				System.out.println(anagram);
				anagram = "";
				printsLeft--;

				if(printsLeft == 0)
					System.out.println("\nStopped at " + totalPrints + " phrases");
			}

			else if(printsLeft > 0 && anagram.split(" ").length < anagramLen)
			{
				for(int i = 0; i < words.size(); i++)
				{
					String newWord = word;
					int len = 0;
					for(int j = 0; j < words.get(i).length(); j++)
					{
						newWord = newWord.replaceFirst(words.get(i).charAt(j) + "", "");
					}
					for(int k = 0; k < anagram.split(" ").length; k++)
					{
						len += anagram.split(" ")[k].length();
					}
					if(len + words.get(i).length() <= mainWord.length())
						findAnagrams(mainWord, newWord, AllWords.findAllWords(newWord, false), anagramLen, anagram + words.get(i) + " ");
				}
			}
			else if(printsLeft > 0)
				anagram = "";
		}
	}

	/**
	 *	Prints the introduction to AnagramMaker
	 */
	public void printIntroduction() {
		System.out.println("\nWelcome to ANAGRAM MAKER\n");
		System.out.println("Provide a word, name, or phrase and out comes their anagrams.");
		System.out.println("You can choose the number of words in the anagram.");
		System.out.println("You can choose the number of anagrams shown.");
		System.out.println("\nLet's get started!\n");
	}
}
