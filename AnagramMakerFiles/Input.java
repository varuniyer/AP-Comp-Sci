/**
 * Interface to represent user input
 *
 * @author Varun Iyer
 * @version 1.18.15
 */

public interface Input
{
	//gets users word to find anagrams for
	String getWord();

	//gets amount of words per anagram
	int getWordsPerPhrase();

	//gets max amount of anagrams to print
	int getMax();

	//removes spaces and punctuation from word
	String parseInput(String word);
}
