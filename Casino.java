import java.util.Scanner;

/**
 * Finds average number off spins for each
 * value of number of sides from 4 to 20 given
 * the amount of trials to run
 *  @author  Varun
 *  @version  8/24/15
 */
public class Casino
{
  public static void main(String[]args)
  {
    int numoftrials = 0;//number of trials
    //object to prompt user how many trials they want to run
    Prompt p = new Prompt();
    numoftrials = p.getInt("The number of trials",10,100000);
    //start rolling
    play(numoftrials);
  }

  /**
   * This method finds the number of trials
   * taken to get 4 dice objects of lengths
   * 4-20 to all have the same value after
   * the inputted number of trials
   *
   * @param int numoftrials, which stores how many trials user wanted
   */
  public static void play(int numoftrials)
  {
    //print the table headers
    System.out.println("Number\n of"+"  Ave number\nsides    of spins");

    //records the number of spins temporarily
    //for each number from 4-20
    long[] arr = new long[numoftrials];

    //average number of spins corresponding
    //with each number of trials based
    //on nums in the avg array
    long[] arrofavgs = new long[17];
    for(int i = 4; i <= 20; i++)
    {
      boolean attained = false;//true if winning combo attained
      int numofspins = 0;//records number of spins so far
      //store the number of rolls it takes per trial to win
      for(int j = 0; j < numoftrials; j++)
      {
        numofspins = 0;
        attained = false;//reset attained to false each trial
        //until user won
        while(!attained)
        {
          //dice objects that we will roll
          Dice die1 = new Dice(i);
          Dice die2 = new Dice(i);
          Dice die3 = new Dice(i);
          Dice die4 = new Dice(i);

          die1.roll();
          die2.roll();
          die3.roll();
          die4.roll();

          if(die1.getValue()==die2.getValue()
           &&die2.getValue()==die3.getValue()
           &&die3.getValue()==die4.getValue())
            attained = true;
          numofspins++;
        }
        arr[j] = numofspins;
      }
      long sum = 0;//sum of number of spins per trial
      //number of spins for all the trials for a single number of sides
      for(int k = 0; k < numoftrials; k++)
      {
        sum += arr[k];
      }
      long avg = sum/numoftrials;//avg of the number of spins per trial
      arrofavgs[i-4] = avg;
    }
    printall(arrofavgs);
  }
  /**
   * This method prints out the average number
   * of rolls needed to get all 4 values when
   * the dice objects have sides 4-20.
   * The info on the averages is stored
   * in the inputted array.
   *
   * @param long[] a, which stores the averages corresponding to
   * when the die has 4-20 sides
   */
  public static void printall(long[] a)
  {
    //loop through printing data for dice with
    //4-20 sides
    for(int i = 4; i <= 20; i++)
    {
      //histogram will have number of asteriks based on
      //the ratio of current element to largest one
      long numofasteriks = (long)(60*((double)a[i-4]/findlargest(a)));
      String asteriks = "";
      for(long k = 0; k < numofasteriks; k++)
      {
        asteriks += "*";
      }
      System.out.printf("%2d: %10d  %s\n",i,a[i-4],asteriks);
    }
  }
  /**
   * This method returns the largest
   * element of the given array.
   *
   * @param long[] a, which can be any array
   * @return long largest, which keeps track of the largest element of the array
   */
  public static long findlargest(long[] a)
  {
    long largest = 0;//represent largest number so far in the array
    for(int i = 0; i < a.length; i++)
    {
      if(a[i] > largest)
        largest = a[i];
    }
    return largest;
  }
}
