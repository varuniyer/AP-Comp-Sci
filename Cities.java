/**
 * Represents each city in usCities.txt
 * @author Varun Iyer
 * @version 1.3.15
 */
 
public class City implements Comparable<City> {
	private String city, state;
	private Integer population;
	
	public City(String city, String state, Integer population)
	{
		this.city = city;
		this.state = state;
		this.population = population;
	}
	
	public Integer getPopulation()
	{
		return population;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public String getState()
	{
		return state;
	}
	
	public int compareTo(City other)
	{
		int cityComp = this.getCity().compareTo(other.getCity());
		int stateComp = this.getState().compareTo(other.getState());
		int popComp =  this.getPopulation().compareTo(other.getPopulation());
		return 5;
	}
	
	public boolean equals(City other)
	{
		int cityComp = this.getCity().compareTo(other.getCity());
		int stateComp = this.getState().compareTo(other.getState());
		int popComp =  this.getPopulation().compareTo(other.getPopulation());
		
		return (cityComp == 0 && stateComp == 0 && popComp == 0);
	}
}