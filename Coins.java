/**
 * Prints all the possiblitities of coins given a number of coins
 *
 * @author Varun Iyer
 * @version 9/6/15
 */

public class Coins
{
   public static void main(String[] args)
   {
      //prompt the number of cents the user wants to find
      //combinations of coins for
      int cents = Prompt.getInt("Total cents");
      printcombos(cents);
   }

   /**
    *
    * Prints out all the possible combinations
    * of coins given the cent value given
    *
    * @param input, the inputted cent value
    */
   public static void printcombos(int input)
   {
      int len = (""+input).length();//digit length of input
      String format = "%"+len+"d";//format to print number of each type of coin

      int c = 0;//current amount of change based on coins in the following loops
      int numofcombos = 0;//number of combinations of coins for the given cent value

      //loops through amount of quarters that can be used to represent the input
      for(int q = 0; q <= input/25; q++)
      {
         //loops through amount of dimes that can be used to represent the input
         for(int d = 0; d <= input/10; d++)
         {
            //loops through amount of nickels that can be used to represent the input
            for(int n = 0; n <= input/5; n++)
            {
               //loops through amount of pennies that can be used to represent the input
               for(int p = 0; p <= input/1; p++)
               {
                  //calculate the change that is currently had with respective amounts of
                  //quarters nickels dimes and pennies
                  c = 25 * q + 10 * d + 5 * n + 1 * p;

                  //print the number of each type of coin
                  //but only if the value of the current change equals the input
                  //use format calculated earlier to account for input length
                  if(c == input)
                  {
                     System.out.printf(format + " = " +
                                       format + " quarters + " +
                                       format + " dimes + " +
                                       format + " nickels + " +
                                       format + " pennies\n", input, q, d, n, p);
                     numofcombos++;
                  }
               }
            }
         }
      }
      System.out.println("\nNumber of Combinations = " + numofcombos);
   }
}
