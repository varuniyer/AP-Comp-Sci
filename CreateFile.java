import java.io.PrintWriter;
import java.io.IOException;
import java.util.Calendar;
public class CreateFile
{
	public void run()
	{
		String fn = Prompt.getString("Name your new Java class");
		PrintWriter p = OpenFile.openToWrite(fn+".java");
		writeheader(p,fn);
	}
	public static void main(String[]args)
	{
		
		CreateFile c = new CreateFile();
		c.run();
	}
	public void writeheader(PrintWriter javafile, String filename)
	{
		String fletter = filename.substring(0,1).toLowerCase();
		Calendar c = Calendar.getInstance();
		javafile.println(
			"/**\n"+
			"* Description\n"+
			"*\n"+
			"* @author Varun Iyer\n"+
			"* @version "+ c.get(Calendar.MONTH)+"."+c.get(Calendar.DATE)+"."+c.get(Calendar.YEAR)+
			"\n*/\n\n"+
			"public class "+filename+"\n"+
			"{\n"+
			"	public static void main(String[]args)\n"+
			"	{\n\t\t"+filename+" "+fletter+" = new "+filename+"();\n\t\t"+
			fletter+".run();\n"+
			"	}\n\n"+
			"	public void run()\n"+
			"	{\n\t\t"+"\n"+
			"	}\n"+
			"}"
		);
		javafile.close();
	}
}