/**
 * Testing the Dice class
 *
 *@author Varun Iyer
 *@version 8/18/15
 */
public class DiceTester
{
	public static void main(String[] args) {
		Dice die1 = new Dice();
		die1.roll();
		System.out.println("\n\ndie1 = "+die1.getValue());
	}
}
