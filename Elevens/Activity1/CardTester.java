/**
 * This is a class that tests the Card class.
 */
public class CardTester {

	/**
	 * The main method in this class checks the Card operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
		/* *** TO BE IMPLEMENTED IN ACTIVITY 1 *** */
		System.out.println("\nc1:");
		Card c1 = new Card("4", "spades", 8);
		System.out.println("Suit: " + c1.suit());
		System.out.println("Rank: " + c1.rank());
		System.out.println("PointValue: " + c1.pointValue());
		System.out.println(c1.matches(new Card("1", "hello", 5)));
		System.out.println(c1.toString());
		
		System.out.println("\nc2:");
		Card c2 = new Card("7", "hearts", 1);
		System.out.println("Suit: " + c2.suit());
		System.out.println("Rank: " + c2.rank());
		System.out.println("PointValue: " + c2.pointValue());
		System.out.println(c2.matches(new Card("1", "hello", 5)));
		System.out.println(c2.toString());
		
		System.out.println("\nc3:");
		Card c3 = new Card("2", "clubs", 4);
		System.out.println("Suit: " + c3.suit());
		System.out.println("Rank: " + c3.rank());
		System.out.println("PointValue: " + c3.pointValue());
		System.out.println(c3.matches(new Card("1", "hello", 5)));
		System.out.println(c3.toString());
	}
}
