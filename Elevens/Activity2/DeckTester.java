/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {

	/**
	 * The main method in this class checks the Deck operations for consistency.
	 *	@param args is not used.
	 */

	public static void main(String[] args) {
		/* *** TO BE IMPLEMENTED IN ACTIVITY 2 *** */

		//vars for small deck
		String[]strNums = {"jack","jack","queen","queen","king","king"};
		String[]suits = {"green","orange","green","orange","green","orange"};
		int[]nums = {11,11,12,12,13,13};
		Deck smallDeck = new Deck(strNums, suits, nums);

		//vars for large deck
		String[]classicSuits = {"spades", "hearts", "diamonds", "clubs"};
		String[]classicNums = {"two","three","four","five",
		                       "six","seven","eight","nine","ten",
		                       "jack","queen","king","ace"};
		int[]classicVals = {2,3,4,5,6,7,8,9,10,10,10,10,11};
		String[]largeSuits = new String[52];
		String[]largeNums = new String[52];
		int[]largeVals = new int[52];
		Deck largeDeck;


		System.out.println("SMALL DECK\n--------------");
		System.out.println("\nOriginal Deck");
		System.out.println(smallDeck.toString());

		smallDeck.deal();
		smallDeck.deal();
		smallDeck.deal();
		smallDeck.deal();
		System.out.println("\n\nAfter four deals:");
		System.out.println(smallDeck.toString());

		smallDeck.deal();
		smallDeck.deal();
		System.out.println("\n\nAfter six deals:");
		System.out.println(smallDeck.toString());


		System.out.println("\n\n\nLARGE DECK\n--------------");
		for(int i = 0; i < largeNums.length; i++)
		{
			largeNums[i] = classicNums[i/4];
			largeSuits[i] = classicSuits[i%4];
			largeVals[i] = classicVals[i/4];
		}
		largeDeck = new Deck(largeNums, largeSuits, largeVals);

		System.out.println("\nOriginal Deck");
		System.out.println(largeDeck.toString());

		largeDeck.deal();
		largeDeck.deal();
		largeDeck.deal();
		System.out.println("\n\nAfter three deals:");
		System.out.println(largeDeck.toString());

		for(int i = 2; i < largeDeck.size(); i++)
		{
			largeDeck.deal();
		}
		System.out.println("\n\nAfter fifty-two deals");
		System.out.println(largeDeck.toString());
	}
}
