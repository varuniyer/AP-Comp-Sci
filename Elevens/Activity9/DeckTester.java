/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {

	/**
	 * The main method in this class checks the Deck operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
		String[] ranks = {"jack", "queen", "king"};
		String[] suits = {"blue", "red", "green"};
		int[] pointValues = {11, 12, 13};
		Deck d = new Deck(ranks, suits, pointValues);

		System.out.println("**** Original Deck Methods ****");
		System.out.println("  toString:\n" + d.toString());
		System.out.println("  isEmpty: " + d.isEmpty());
		System.out.println("  size: " + d.size());
		System.out.println();
		System.out.println();

		System.out.println("**** Deal a Card ****");
		System.out.println("  deal: " + d.deal());
		System.out.println();
		System.out.println();

		System.out.println("**** Deck Methods After 1 Card Dealt ****");
		System.out.println("  toString:\n" + d.toString());
		System.out.println("  isEmpty: " + d.isEmpty());
		System.out.println("  size: " + d.size());
		System.out.println();
		System.out.println();

		System.out.println("**** Deal Remaining 5 Cards ****");
		for (int i = 0; i < 5; i++) {
			System.out.println("  deal: " + d.deal());
		}
		System.out.println();
		System.out.println();

		System.out.println("**** Deck Methods After All Cards Dealt ****");
		System.out.println("  toString:\n" + d.toString());
		System.out.println("  isEmpty: " + d.isEmpty());
		System.out.println("  size: " + d.size());
		System.out.println();
		System.out.println();

		System.out.println("**** Deal a Card From Empty Deck ****");
		System.out.println("  deal: " + d.deal());
		System.out.println();
		System.out.println();

		/* *** TO BE COMPLETED IN ACTIVITY 4 *** */

		String[]classicSuits = {"spades", "hearts", "diamonds", "clubs"};
		String[]classicNums = {"two","three","four","five",
		                       "six","seven","eight","nine","ten",
		                       "jack","queen","king","ace"};
		int[]classicVals = {2,3,4,5,6,7,8,9,10,10,10,10,11};
		String[]largeSuits = new String[52];
		String[]largeNums = new String[52];
		int[]largeVals = new int[52];
		Deck largeDeck;
		System.out.println("\n\n\nLARGE DECK\n--------------");
		for(int i = 0; i < largeNums.length; i++)
		{
			largeNums[i] = classicNums[i/4];
			largeSuits[i] = classicSuits[i%4];
			largeVals[i] = classicVals[i/4];
		}
		largeDeck = new Deck(largeNums, largeSuits, largeVals);

		System.out.println("\nOriginal Deck");
		System.out.println(largeDeck.toString());

		largeDeck.shuffle();
		largeDeck.shuffle();
		largeDeck.shuffle();
		System.out.println("\n\nAfter three shuffles:");
		System.out.println(largeDeck.toString());

		for(int i = 2; i < largeDeck.size(); i++)
		{
			largeDeck.shuffle();
		}
		System.out.println("\n\nAfter fifty-two shuffles");
		System.out.println(largeDeck.toString());
	}
}
