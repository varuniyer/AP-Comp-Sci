import java.util.ArrayList;
import java.util.regex.*;

/**
 * Represents the list of all
 * IndexEntries for a given
 * document
 *
 * @author Varun Iyer
 * @version 11/23/15
 */

public class DocumentIndex extends ArrayList<IndexEntry>
{
	/**
	 * Sets List length to default
	 */

	public DocumentIndex()
	{
		super();
	}

	/**
	 * Sets ArrayList length to given
	 *
	 * @param len  Length given for ArrayList
	 */

	public DocumentIndex(int len)
	{
		super(len);
	}

	/**
	 * Adds num to the IndexEntry for
	 * word by calling that IndexEntry's
	 * add(num) method and creates an
	 * IndexEntry for new words
	 *
	 * @param word  Word in document
	 * @param num  Line number of word
	 */

	public void addWord(String word, int num)
	{
		boolean containsWord = false;

		for(IndexEntry i: this)
		{
			if(i.getWord().equals(word))
			{
				i.add(num);
				containsWord = true;
				break;
			}
		}

		if(!containsWord)
		{
			int count = 0;
			for(IndexEntry i: this)
			{
				if((int)(i.getWord().charAt(0)) > (int)word.charAt(0))
				{
					this.add(count, new IndexEntry(word));
					containsWord = true;
					break;
				}
				count++;
			}

			if(!containsWord)
			{
				this.add(new IndexEntry(word));
			}
		}
	}

	/**
	 * Extracts all the words from str
	 * and calls addWord(word, num) for
	 * each word
	 *
	 * @param str  given phrase for extraction
	 * @param num  line number of given phrase
	 */

	public void addAllWords(String str, int num)
	{
		for(String s : str.split("\\W+"))
		{
			if(!s.equals(""))
				addWord(s, num);
		}
	}
}
