import java.util.ArrayList;

/**
 * represents an index entry of the document index
 *
 * @author Varun Iyer
 * @version 11/23/15
 */

public class IndexEntry
{
	private String word; //Word in document
	private ArrayList<Integer> numsList; //List of line numbers per word

	/**
	 * Sets word as uppercase input and sets
	 * numList to default integer arraylist value
	 *
	 * @param w  Word passed to constructor
	 */

	public IndexEntry(String w)
	{
		word = w.toUpperCase();
		numsList = new ArrayList<Integer>();
	}

	/**
	 * Appends num to numList if it
	 * is not already in the list
	 *
	 * @param num  Line number to be appended
	 */

	public void add(int num)
	{
		if(numsList.contains((Integer)(num)))
			numsList.add((Integer)(num));
	}

	/**
	 * returns word
	 *
	 * @return word  Word in document
	 */

	public String getWord()
	{
		return word;
	}

	/**
	 * Returns a string representation
	 * of this IndexEntry in the output
	 * format
	 *
	 * @return formatted String representation
	 */

	public String toString()
	{
		String formatted = word;
		for(Integer i: numsList)
		{
			formatted += " " + (int)i + ",";
		}
		return formatted;
	}
}
