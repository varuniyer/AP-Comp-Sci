import java.util.Scanner;
import java.io.PrintWriter;

/**
 * searches a top 1000 names database for the inputted name
 * through the decades from 1900 - 2010
 *
 * @author Varun Iyer
 * @version 8/28/15
 */

public class Names
{
	public static void main(String[]args)
	{
		//initialize scanner for the directory of names, names.txt
		Scanner namesdir = OpenFile.openToRead("names.txt");
		String prompt = "Please enter the name you're searching for:";
		String name = Prompt.getString(prompt);
		getNameData(namesdir,name);
	}

	/**
	 * This method finds the name in the directory
	 * and generates an array containing the ranking
	 * of the inputted name each decade.
	 *
	 * @param dir, the directory of names
	 * @param name, the name of the person searched for
	 */
	public static void getNameData(Scanner dir, String name)
	{
		//header of what will be the table
		String header = dir.nextLine();
		//first year of recorded ranking
		int begyear = Integer.parseInt(header.substring(6,10));
		//last year of recorded ranking
		int endyear = Integer.parseInt(header.substring(header.length()-6,header.length()-2));
		//difference of the two divided by ten is the amount of decades
		int len = (endyear - begyear)/10 + 1;
		//make a rank array containing the ranking of the person in each of these decades
		int[]rank = new int[len];

		//searches for name in directory and appends corresponding ranks to the rank array
		//breaks after it finds the line containing the right name
		while(dir.hasNext())
		{
			if(name.equalsIgnoreCase(dir.next()))
			{
				for(int i = 0; i < len; i++)
				{
					rank[i] = dir.nextInt();
				}
				break;
			}
		}
		//print results using gathered data
		printTable(header, name, rank);
	}

	/**
	 * Prints a the data in a table format for each decade
	 *
	 * @param h, header the table
	 * @param n, the name being searched for
	 * @param r, the array containing the ranking for each name throughout the decades
	 *
	 */
	public static void printTable(String h, String n, int[]r)
	{
		//only try to gather data if there is any in the rank array
		if(!arrisempty(r))
		{
			//print out name followed by the corresponding rankings separated by two spaces
			System.out.print("\n" + capfirstletter(n));
			for(int i = 0; i < r.length; i++)
			{
				System.out.print("   " + r[i]);
			}
			System.out.print("\n\n\n      " + h.substring(6,h.length()-1));

			//print out numbers 20 - 1000 as a scale
			//then place the rankings for each decade
			//on the row of the largest number they are
			//greater than
			for(int j = 20; j <= 1000; j += 20)
			{
				System.out.printf("\n%4d",j);
				for(int k = 0; k < r.length; k++)
				{
					//place number in scale based on whether it is smaller than the current number
					//and based on whether or not it has already been placed or is not on the list
					//at a certain decade
					if(r[k] < j && r[k] != 0)
					{
						System.out.printf("   %3s",r[k]);//place on the list
						r[k] = 0;//make it so the rank can't be placed again
					}
					else
					{
						System.out.print("   ---");//fill with a gap if not on the list
					}
				}
			}
			System.out.println();
		}
		//otherwise say the name couldn't be found
		else
			System.out.println("\n"+capfirstletter(n)+" not found in database.\n");
	}

	/**
	 * checks if array values have been initialized yet
	 * by seeing if all the values are 0
	 *
	 * @param a, the array to be observed
	 * @return empty, whether or not all values are 0
	 */
	public static boolean arrisempty(int[]a)
	{
		boolean empty = true;
		//assumes it is empty and says otherwise
		//once it finds a non-zero element in the
		//array
		for(int i = 0; i < a.length; i++)
		{
			if(a[i] != 0)
				empty = false;
		}
		return empty;
	}

	/**
	 * Capitalizes the first letter of the inputted string
	 *
	 * @param s, the string the user wants to capitalize the first letter of
	 * @return capstr, the string with the capitalized first letter
	 */
	public static String capfirstletter(String s)
	{
		//adds first character of string capitalized to the rest of the string
		String capstr = ("" + s.charAt(0)).toUpperCase()+s.substring(1,s.length());
		return capstr;
	}
}
