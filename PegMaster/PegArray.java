/**
 * Gets and sets values from Array
 * containing the set of pegs of both
 * the user's guesses and the computer
 *
 * @author Varun Iyer
 * @version 10.11.15
 */

public class PegArray
{
	private char[]arr = new char[4];
	private int pmcount, emcount;
	private char[]colors = {'B', 'W', 'B', 'G', 'Y', 'R'};//black white blue green yellow red

	public PegArray()
	{
		this.arr[0] = 'X';
		this.arr[1] = 'X';
		this.arr[2] = 'X';
		this.arr[3] = 'X';
	}

	public char getPeg(int i)
	{
		return this.arr[i];
	}

	public void setPeg(int i, boolean prompt)
	{
		char c;
		if(prompt)
			c = Prompt.getChar("Please give the first letter of the color you\n"+
		                        "would like to select for the "+(i+1)+"th peg.");
		else
		{
			c = colors[(int)(Math.random()*6)];
			this.arr[i] = c;
		}
		while(this.arr[i] != c)
		{
			for(int j = 0; j < colors.length; j++)
				if(colors[j] == c)
					this.arr[i] = c;
			if(this.arr[i] != c)
				c = Prompt.getChar("Please give the first letter of the color you\n"+
			                   "would like to select for the "+(i+1)+"th peg.");
			System.out.println();
		}
	}

	public int getLength()
	{
		return this.arr.length;
	}

	public int getExactMatches(PegArray pegArr)
	{
		this.emcount = 0;
		for (int i = 0; i < this.arr.length; i++)
		{
			if(this.arr[i] == pegArr.arr[i])
				this.emcount++;
		}
		return this.emcount;
	}


	public int getPartialMatches(PegArray pegArr)
	{
		this.pmcount = 0;
		for (int i = 0; i < this.arr.length; i++)
		{
			for (int j = 0; j < pegArr.arr.length; j++)
			{
				//	if ((i != j) && (this.arr[i] != pegArr.arr[i]) && (this.arr[j] != pegArr.arr[j]) && (this.arr[j] != 'X'))
					this.pmcount++;
			}
		}
		return this.pmcount;
	}
}
