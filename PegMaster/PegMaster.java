/**
 * PegMaster Game in which user guesses
 * the color of the pegs selected by the computer
 *
 * @author Varun Iyer
 * @version 10.8.15
 */

public class PegMaster {

	boolean reveal;			// whether to reveal the master combination
	PegArray[] pegArray;	// the array of guess peg arrays
	PegArray master;		// the master (key) peg array

	public void printIntroduction() {
		System.out.println("\n");
		System.out.println("+------------------------------------------------------------------------------------+");
		System.out.println("| WELCOME TO MONTA VISTA PEGMASTER!                                                  |");
		System.out.println("|                                                                                    |");
		System.out.println("| The game of PegMaster is played on a four-peg gameboard, and six peg colors can    |");
		System.out.println("| be used.  First, the computer will choose a random combination of four pegs, using |");
		System.out.println("| some of the six colors (black, white, blue, green, yellow, and red).  Repeats are  |");
		System.out.println("| allowed, so there are 6 * 6 * 6 * 6 = 1296 possible combinations.  This \"master    |");
		System.out.println("| combination\" is then hidden from the player, and the player starts making guesses  |");
		System.out.println("| at the master combination.  The player has 10 turns to guess the combination.      |");
		System.out.println("| Each time the player makes a guess for the 4-peg combination, the number of exact  |");
		System.out.println("| matches and partial matches are then reported back to the user. If the player      |");
		System.out.println("| finds the exact combination, the game ends with a win.  If the player does not     |");
		System.out.println("| find the master combination after 10 turns, the game ends with a loss.             |");
		System.out.println("|                                                                                    |");
		System.out.println("| LET'S PLAY SOME PEGMASTER!                                                         |");
		System.out.println("+------------------------------------------------------------------------------------+");
		System.out.println("\n");
	}

	/**
	 * Prints current state of game including
	 * the pegs guessed exactly, partially
	 * correctly
	 */
	public void printBoard() {
		System.out.println();
		System.out.println("+----------------------------------------------------------------------+");
		System.out.println("| MASTER      1     2     3     4     5     6     7     8     9     10 |");
		System.out.println("+-------+  +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+");
		for (int row = 0; row < 4; row++) {

			//reveal the master pegs
			if (reveal) System.out.printf("|   %c   |  |", master.getPeg(row));
			else System.out.print("|  ***  |  |");
			for (int col = 0; col < 10; col++) {
				char c = pegArray[col].getPeg(row);
				if (c != 'X') System.out.printf("  %c  |", c);
				else System.out.printf("     |");
			}
			System.out.println();
			System.out.println("+-------+  +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+");
		}
		System.out.print("| Exact    ");
		for (int col = 0; col < 10; col++) {
			int num = pegArray[col].getExactMatches(master);
			if (num > -1) System.out.printf("   %d  ", num);
			else System.out.printf("      ");
		}
		System.out.println("|");
		System.out.print("| Partial  ");
		for (int col = 0; col < 10; col++) {
			int num = pegArray[col].getPartialMatches(master);
			if (num > -1) System.out.printf("   %d  ", num);
			else System.out.printf("      ");
		}
		System.out.println("|");
		System.out.println("+----------------------------------------------------------------------+");
		System.out.println();
	}

	public PegArray[] setUpPegs(PegArray[] arrofarrs)
	{
		arrofarrs = new PegArray[10];
		for(int i = 0; i < arrofarrs.length; i++)
		{
			arrofarrs[i] = new PegArray();
		}
		return arrofarrs;
	}

	public PegArray setUpMaster(PegArray arr)
	{
		arr = new PegArray();

		for(int i = 0; i < arr.getLength(); i++)
		{
			arr.setPeg(i,false);
		}
		return arr;
	}

	public void run()
	{
		printIntroduction();

		pegArray = setUpPegs(pegArray);
		master = setUpMaster(master);
		int currentTry = 0;

		while(pegArray[9].getPeg(0) == 'X')
		{
			printBoard();

			for(int i = 0; i < pegArray[currentTry].getLength(); i++)
				pegArray[currentTry].setPeg(i,true);

			if(pegArray[currentTry].getExactMatches(master) == 4)
			{
				reveal = true;
				printBoard();
				System.out.println("Congratulations! You won in " + (currentTry + 1) + " turns!\n");
				System.out.print("Master combo:");
				for(int i = 0; i < master.getLength(); i++)
				{
					System.out.print(" " + master.getPeg(i) + " ");
				}
				System.out.println("\n");
				break;
			}
			else if(currentTry == 9)
			{
				reveal = true;
				printBoard();
				System.out.println("You Lost! Better Luck Next Time!\n");
				System.out.print("Master combo:");
				for(int i = 0; i < master.getLength(); i++)
				{
					System.out.print(" " + master.getPeg(i) + " ");
				}
				System.out.println("\n");
				break;
			}
			currentTry++;
		}
	}

	public static void main(String[]args)
	{
		PegMaster pm = new PegMaster();
		pm.run();
	}
}
