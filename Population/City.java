/**
 * Represents each city in usCities.txt
 * @author Varun Iyer
 * @version 1.3.15
 */

public class City implements Comparable<City> {
	private String city, state;
	private Integer population;

	/**
	 * Constructs a city given city name, state, and population
	 * @param city  String containing city name
	 * @param state  String containing state name
	 * @param population  Integer containing population
	 */
	public City(String city, String state, Integer population)
	{
		this.city = city;
		this.state = state;
		this.population = population;
	}

	/**
	 * returns city's population
	 * @return  population
	 */
	public Integer getPopulation()
	{
		return population;
	}

	/**
	 * returns city name
	 * @return city  city name
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * returns state name
	 * @return state  state name
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * compares cities with the following priority:
	 * 1. City Name
	 * 2. State Name
	 * 3. Population
	 * @param other  city being compared to
	 * @return cityComp  compareTo for citie names
	 * @return stateComp  compareTo for state names
	 * @return popComp  compareTo for population
	 */
	public int compareTo(City other)
	{
		int cityComp = this.getCity().compareTo(other.getCity());
		int stateComp = this.getState().compareTo(other.getState());
		int popComp =  this.getPopulation().compareTo(other.getPopulation());
		if(cityComp == 0)
			if(stateComp == 0)
				return popComp;
			else
				return stateComp;
		else
			return cityComp;
	}

	/**
	 * checks if city, state, and population
	 * are the same between indicated cities
	 * @param other  city being compared to
	 * @return isEqual whether they are equal
	 */
	public boolean equals(City other)
	{
		int cityComp = this.getCity().compareTo(other.getCity());
		int stateComp = this.getState().compareTo(other.getState());
		int popComp =  this.getPopulation().compareTo(other.getPopulation());

		boolean isEqual = (cityComp == 0 && stateComp == 0 && popComp == 0);
		return isEqual;
	}
}
