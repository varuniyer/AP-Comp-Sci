
/**
 * Searches for desired city from usCities.txt
 * @author Varun Iyer
 * @version 1.3.15
 */
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Population
{
    private ArrayList<City> cityDB;

    /**
     * Performs a binary search with cities
     * to find all city name matches
     * @param a  ArrayList of cities to be searched through
     * @param userCity City name to be compared to
     * @return matches  arraylist of indices containing occurences of the inputted city
     */
    public ArrayList<Integer> binarySearch( ArrayList<City> a, String userCity )
    {
        int low = 0;
        int high = a.size() - 1;
        int mid;
        ArrayList<Integer> matches = new ArrayList<Integer>();

        while( low <= high )
        {
            mid = ( low + high ) / 2;

            if( a.get(mid).getCity().compareTo( userCity ) < 0 )
                low = mid + 1;
            else if( a.get(mid).getCity().compareTo( userCity ) > 0 )
                high = mid - 1;
            else
            {
                for(int i = mid; i >= 0; i--)
                {
                    if(a.get(i).getCity().equals(userCity))
                        matches.add((Integer)i);
                    else
                        break;
                }

                for(int j = mid + 1; j < a.size(); j++)
                {
                    if(a.get(j).getCity().equals(userCity))
                        matches.add((Integer)j);
                    else
                        break;
                }
                return matches;
            }
        }
        return matches;
    }

    /**
     * Reads Database File, usCities.txt, and
     * builds the cityDB arraylist using the
     * file's listed cities as elements
     * Finally the list is sorted
     */
    public void setUpDb()
    {
        Scanner dbFile = OpenFile.openToRead("usCities.txt");
        cityDB = new ArrayList<City>();

        System.out.println("\nReading database from file usCities.txt\n");
        while(dbFile.hasNextLine())
        {
            String[] cityInfo = dbFile.nextLine().split(",");
            String city = cityInfo[0];
            String state = cityInfo[1];
            Integer pop = Integer.parseInt(cityInfo[2]);
            cityDB.add(new City(city, state, pop));
        }

        System.out.println("Sorting database\n");
        Sorts.mergeSort(cityDB, 0, cityDB.size() - 1);
    }

    /**
     * Searches for user inputted city
     * in database and prints out city,
     * state, and population of all
     * cities with the inputted name
     * It continues to prompt the user
     * until he or she presses "q"
     * to quit the program
     */
    public void matchInput()
    {
        String city = Prompt.getString("City name to find (q to Quit)");
        if(!city.equals("q"))
        {
            ArrayList<City> matches = new ArrayList<City>();

            for(Integer i : binarySearch(cityDB, city))
            {
                matches.add(cityDB.get(i));
            }

            Sorts.mergeSort(matches, 0, matches.size() - 1);

            System.out.println("\nCities that match:");
            System.out.printf("    %-16s%-4s%15s\n","City","State","Population");
            System.out.printf("    %-16s%-5s%15s\n","--------------","-----","----------");

            for(City c : matches)
            {
                System.out.printf("    %-18s%-4s%14s\n", c.getCity(), c.getState(), ""+c.getPopulation());
            }
            System.out.println("\n");
            matchInput();
        }
    }

    // Run program
    public void run()
    {
        setUpDb();
        matchInput();
    }

    public static void main( String [ ] args )
    {
        Population p = new Population();
        p.run();
    }
}
