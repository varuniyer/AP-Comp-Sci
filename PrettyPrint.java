import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;

/**
* Prints and reformats inputted java source file
*
* @author Varun Iyer
* @version 10.22.2015
*/

public class PrettyPrint
{
	// Fields
	private final String TAB = "    ";
	private int indent;
	private String contents = "";     // contains the whole program in one string
	private String formatted = "";    // contains the updated formatted program in one string
	private String sentence;          // current sentence
	private String token;		      // current token
	private Scanner javaSrc;	      // scanner for the java source file
	private Scanner scanContents;     // scanner for the contents string
	private PrintWriter testFile;     // printwriter for file that will contain the formatted program

	public static void main(String[]args)
	{
		PrettyPrint p = new PrettyPrint();
		p.run(args);
	}

	public void run(String[]args)
	{
		javaSrc = OpenFile.openToRead(args[0]);
		stripContents();
		parseContents();
	}

	/**
	 *  Strips contents of any newlines or tab characters
	 *  and isolate brackets, semicolons, comment starters(/**),
	 *  and comment enders
	 */
	public void stripContents() {
		// Strip newlines
		while(javaSrc.hasNextLine())
		{
			contents += javaSrc.nextLine();
		}
		// Strip tab characters and replace with spaces
		for(int i = 0; i < contents.length(); i++)
		{
			if(contents.charAt(i) == '\t')
				replaceContentsChar(i,TAB);
		}
		// Isolate braces
		//System.out.println(contents.length());
		isolateChar('{', 2);
		isolateChar('}', 2);

		// Isolate semicolons
		isolateChar(';', 1);

		// Isolate comment starters(/**)
		isolateString("/**");

		// Isolate comment enders
		isolateString("*/");

		scanContents = new Scanner(contents);
	}

	/**
	 *  Parses the contents
	 */
	public void parseContents()
	{
		testFile = OpenFile.openToWrite("Test.java");
		int indent = 0;
		while(scanContents.hasNext())
		{
			sentence = scanContents.next();
			if(sentence.equals("{"))
			{
				parseSentence(indent);
				indent++;
			}
			else if(sentence.equals("}"))
			{
				indent--;
				parseSentence(indent);
				if(indent == 1)
					formatted += "\n";
			}
			else
				parseSentence(indent);
		}
		formatted = formatted.substring(0, formatted.length() - 2) + formatted.charAt(formatted.length() - 1);
		System.out.println(formatted);
		testFile.println(formatted);
		testFile.close();
	}

	/**
	 *  Parses one line at a time. Calls itself recursively as it indents.
	 *
	 *  @param indent The level of indentation
	 */
	public void parseSentence(int indent)
	{
		String totaltab = "";
		for(int i = 0; i < indent; i++)
		{
			totaltab += TAB;
		}
		if(sentence.equals("import"))
			formatted += "import " + lookAhead() + "\n";

		else if(sentence.equals("/**"))
			parseComment(totaltab);

		else
		{
			parseToken(totaltab);
		}
	}

	/**
	 *  Parses one token from variable contents
	 *
	 *  @return The token string
	 */
	public void parseToken(String tab)
	{
		if(sentence.length() >= 3 && sentence.indexOf("for") != -1)
		{
			while(sentence.indexOf(')') == -1 || numOf(sentence,"(") != numOf(sentence,")"))
			{
				sentence += " " + lookAhead();
			}
		}

		if (formatted.substring(formatted.length() - 2).equals("*/"))
			formatted += "\n" + tab + sentence;

		else if(sentence.equals("public") && formatted.substring(formatted.length() - 2).equals("}\n"))
			formatted += "\n" + tab + sentence;

		else if(sentence.equals("+") || sentence.equals("-") || sentence.equals("*") || sentence.equals("/"))
			formatted += " " + sentence;

		else if (sentence.equals("{"))
			formatted += "\n" + tab + sentence;

		else if (sentence.equals("}"))
			formatted += "\n" + tab + sentence;

		else if (numOf(formatted,"(") == numOf(formatted,")") &&
		         formatted.charAt(formatted.length() - 1) == ')')
			formatted += "\n" + tab + TAB + sentence;

		else if (formatted.charAt(formatted.length() - 1) == ';' ||
		         formatted.charAt(formatted.length() - 1) == '{' ||
		         formatted.charAt(formatted.length() - 1) == '}')
			formatted += "\n" + tab + sentence;
		else
		{
			if(formatted.charAt(formatted.length() - 1) != '\n')
				formatted += " ";
			formatted += sentence;
		}
	}

	/**
	 *  Parses one token ahead from variable contents without moving index.
	 *
	 *  @return The next token in contents
	 */
	public String lookAhead()
	{
		if(scanContents.hasNext())
			token = scanContents.next();
		return token;
	}

	/**
	 *  Parses comment blocks
	 *
	 *  @param indent The level of indentation
	 */
	public void parseComment(String tab)
	{
		formatted += "\n" + tab + "/**";
		lookAhead();
		while(token.equals("*"))
		{
			formatted += "\n" + tab + " " + token;
			lookAhead();
			//System.out.println("formatted "+formatted);
			while(!(token.charAt(0) == '*'))
			{
				//System.out.println("token: "+token);
				if(!(token.equals("*/")))
				{
					formatted += " " + token;
					lookAhead();
				}
			}
		}
		formatted += "\n" + tab + " */";
	}

	/**
	 * Isolates a character by surrounding it with one or two spaces
	 * and then replaces the original character in contents with
	 * the string created by adding surrounding spaces
	 *
	 * @param symbol    The character to be isolated
	 * @param spacenum  The number of spaces surrounding symbol
	 */
	public void isolateChar(char symbol, int spacenum)
	{
		for(int i = 0; i < contents.length(); i++)
		{
			if(contents.charAt(i) == symbol)
			{
				if(spacenum == 1)
					replaceContentsChar(i, symbol + " ");
				else if(spacenum == 2)
					replaceContentsChar(i, " " + symbol + " ");
				i++;
			}
		}
	}

	public void isolateString(String symbol)
	{
		for(int i = 0; i < contents.length() - symbol.length() + 1; i++)
		{
			if(contents.substring(i, i + symbol.length()).equals(symbol))
			{
				if(i == 0)
					contents = " " + symbol + " " + contents.substring(i + symbol.length());
				else if(i == contents.length() - symbol.length())
					contents = contents.substring(0,i) + " " + symbol + " ";
				else
					contents = contents.substring(0,i) + " " + symbol + " " + contents.substring(i + symbol.length());
				i += 2;
			}
		}
	}
	/**
	 * Replaces a character in the content string at
	 * the given index with the given string to replace with
	 *
	 * @param index  The index of the character to be replaced
	 * @param s      The string that will replace the char at index
	 */
	public void replaceContentsChar(int index, String s)
	{
		if(index == 0)
			contents = s + contents.substring(index + 1);
		else if(index == contents.length() - 1)
			contents = contents.substring(0, index) + s;
		else
			contents = contents.substring(0, index) + s + contents.substring(index + 1);
	}

	/**
	 * Finds the number of occurences of a
	 * given substring in the given string
	 *
	 * @param str     The string in which the substr occurs a certain number of times
	 * @param substr  The substring whose frequency in str is to be tracked
	 */
	public int numOf(String str, String substr)
	{
		int numOf = 0;
		for(int i = 0; i < (str.length() - substr.length() + 1); i++)
		{
			if(str.substring(i,i+substr.length()).equals(substr))
				numOf++;
		}
		return numOf;
	}
}
