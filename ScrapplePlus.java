/**
 *  A simple version of the Scrabble game where the user plays against the computer.
 *
 *  @author Varun Iyer
 *  @version 9/21/15
 */

public class ScrapplePlus {

	public static final int NUMTILES = 8;//number of tiles in eachplayer's hand

	public int [] scores = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10,
	                        1, 1, 1, 1, 4, 4, 8, 4, 10};//array containing score per letter in alpha order
	private String tilesRemaining = " AAAAAAAAAABBCCDDDDEEEEEEEEEEEEEFFGGGHHIIIIIIIII" +
	                                "JKLLLLMMNNNNNNOOOOOOOOPPQRRRRRRSSSSTTTTTTUUUUVVWWXYYZ ";//string containing which tiles are remaining
	public int userScore = 0;//overall user score
	public int cpuScore = 0;//overall cpu score
	public String userTiles = " A A A A A A A A ";//sets initial value for tiles in user's hand
	public String cpuTiles = " A A A A A A A A ";//sets initial value for tiles in cpu's hand

	// Constructor
	public ScrapplePlus() {
	}

	public static void main(String [] args) {
		ScrapplePlus sp = new ScrapplePlus();
		sp.playGame();
	}

	public void playGame() {
		//print intro teaching user how to play
		printIntroduction();
		//get new set of tiles from tilesremaining for user and cpu
		setUpTiles();
		String prevUserWord = " ";//previous word used by user
		String prevCpuWord = " ";//previous word used by cpu
		String word;//word given by cpu or user
		String player;//who's turn is it
		String[]words = new String[1000];//array containing all the possible words cpu can create
		// Play the game
		boolean endOfGame = false;
		do {
			word = "thisisntaword";

			// User's turn
			player = "User";
			//print tiles remaining, scores of user/cpu, and tiles in user/cpu's hands
			printInfo();
			//get the word from the user
			word = getWord(player);
			System.out.println();
			userScore += 2*WordUtilities.getScore(word,scores,true,prevUserWord);
			System.out.println("BONUS WORD SCORE!!!");
			//replace the tiles used with those from remaining tiles
			for(int i = 0; i < word.length(); i++)
			{
				replaceTile(player, userTiles.toLowerCase().indexOf(word.charAt(i)));
			}
			prevUserWord = word;

			// Computer's turn
			word = "thisisntaword";
			//prompt user to end turn and begin that of cpu
			Prompt.getString("It's the computer's turn. Hit ENTER on the keyboard to continue:");
			player = "Computer";
			//print tiles remaining, scores of user/cpu, and tiles in user/cpu's hands
			printInfo();
			//find best possible word for cpu and print it out
			word = WordUtilities.bestWord(WordUtilities.findAllWords(cpuTiles.toLowerCase()),scores);
			System.out.println("The computer chose: " + word);
			cpuScore += WordUtilities.getScore(word,scores,true,prevCpuWord);
			//replace the tiles used with those from remaining tiles
			for(int i = 0; i < word.length(); i++)
			{
				replaceTile(player, cpuTiles.toLowerCase().indexOf(word.charAt(i)));
			}
			prevCpuWord = word;
			//if there are no more vowels end the game
			if(tilesRemaining.indexOf('A') == -1 &&
			   tilesRemaining.indexOf('E') == -1 &&
			   tilesRemaining.indexOf('I') == -1 &&
			   tilesRemaining.indexOf('O') == -1 &&
			   tilesRemaining.indexOf('U') == -1)
			{
				endOfGame = true;
				endGamePrint();
			}

		} while (! endOfGame);
	}


	/**
	 *  Print the introduction screen for Scrapple.
	 */
	public void printIntroduction() {
		System.out.print(" _______     _______     ______     ______    ");
		System.out.println(" ______    ______   __          _______");
		System.out.print("/\\   ___\\   /\\  ____\\   /\\  == \\   /\\  __ \\   ");
		System.out.println("/\\  == \\  /\\  == \\ /\\ \\        /\\  ____\\");
		System.out.print("\\ \\___   \\  \\ \\ \\____   \\ \\  __<   \\ \\  __ \\  ");
		System.out.println("\\ \\  _-/  \\ \\  _-/ \\ \\ \\_____  \\ \\  __\\");
		System.out.print(" \\/\\______\\  \\ \\______\\  \\ \\_\\ \\_\\  \\ \\_\\ \\_\\ ");
		System.out.println(" \\ \\_\\     \\ \\_\\    \\ \\______\\  \\ \\______\\");
		System.out.print("  \\/______/   \\/______/   \\/_/ /_/   \\/_/\\/_/ ");
		System.out.println("  \\/_/      \\/_/     \\/______/   \\/______/ TM");
		System.out.println();
		System.out.print("This game is a modified version of Scrabble. ");
		System.out.println("The game starts with a pool of letter tiles, with");
		System.out.println("the following group of 100 tiles:\n");

		for (int i = 1; i < tilesRemaining.length(); i ++) {
			System.out.printf("%c ", tilesRemaining.charAt(i));
			if (i == 49) System.out.println();
		}
		System.out.println("\n");
		System.out.printf("The game starts with %d tiles being chosen at ran", NUMTILES);
		System.out.println("dom to fill the player's hand. The player must");
		System.out.printf("then create a valid word, with a length from 4 to %d ", NUMTILES);
		System.out.println("letters, from the tiles in his/her hand. The");
		System.out.print("\"word\" entered by the player is then checked. It is first ");
		System.out.println("checked for length, then checked to make ");
		System.out.print("sure it is made up of letters from the letters in the ");
		System.out.println("current hand, and then it is checked against");
		System.out.print("the word text file. If any of these tests fail, the game");
		System.out.println(" terminates. If the word is valid, points");
		System.out.print("are added to the player's score according to the following table ");
		System.out.println("(These scores are taken from the");
		System.out.println("game of Scrabble):");

		// Print line of letter scores
		char c = 'A';
		for (int i = 0; i < 26; i++) {
			System.out.printf("%3c", c);
			c = (char)(c + 1);
		}
		System.out.println();
		for (int i = 0; i < scores.length; i++) System.out.printf("%3d", scores[i]);
		System.out.println("\n");

		System.out.print("The score is doubled if the word has consecutive double ");
		System.out.println("letters (e.g. ball). The score can also");
		System.out.print("double if the first character of the word follows the ");
		System.out.println("first character of the last word entered");
		System.out.print("in alphabetical order (e.g. \"catnip\" gets ");
		System.out.println("regular score, followed by \"dogma\" which earns double");
		System.out.print("points). If the word contains both, then quadruple the ");
		System.out.println("points.\n");

		System.out.print("Once the player's score has been updated, more tiles are ");
		System.out.println("chosen at random from the remaining pool");
		System.out.printf("of letters, to fill the player's hand to %d letters. ", NUMTILES);
		System.out.println("The player again creates a word, and the");
		System.out.print("process continues. The game ends when the player enters an ");
		System.out.println("invalid word, or the letters in the");
		System.out.println("pool and player's hand run out. Ready? Let's play!\n");

		Prompt.getString("HIT ENTER on the keyboard to continue:");

	}

	public String getWord(String player)
	{
		String w = "thiswontbeinputted";//word is initialized to default value so errors are not given before user input
		//while the word is not a real word or if it is not from the user's tiles
		while(!(WordUtilities.isWord(w) && isFromTiles(w)))
		{
			//check if the word has been re-initialized from default value
			if(!w.equals("thiswontbeinputted"))
			{
				//tell what the user did wrong, if need be
				if(!WordUtilities.isWord(w))
					System.out.println("Your word needs to be from wordlist.txt. Try again.");
				if(!isFromTiles(w))
					System.out.println("Your word must be composed of letters from your pile.");
			}
			//let user input a word again to be checked
			w = WordUtilities.getInput();
		}
		return w;
	}

	public boolean isFromTiles(String w)
	{
		boolean isFromTiles = false;//checks if word is made from tiles
		//try match the letters from user's word to those of his or her set of tiles

		if(WordUtilities.matchletters(w,userTiles.toLowerCase()))
				isFromTiles = true;
		return isFromTiles;
	}

	public void setUpTiles()
	{
		//loops through tiles of user and cpu
		//replacing each their tiles
		for(int i = 1; i < userTiles.length(); i+=2)
		{
			replaceTile("User", i);
		}

		for(int i = 1; i < cpuTiles.length(); i+=2)
		{
			replaceTile("Computer", i);
		}
	}

	public void printInfo()
	{
		int boxwidth = 20;//width of box showing tiles remaining
		System.out.println("\nHere are the tiles remaining in the pool of letters:\n");
		//make newline every time boxwidth letters have been printed to format tiles remaining printout
		for(int i = 1; i < tilesRemaining.length() - 1; i++)
		{
			if((i+boxwidth+1)%boxwidth == 0)
				System.out.println();
			System.out.print(tilesRemaining.charAt(i)+" ");
		}
		System.out.println("\n");

		//format and print scores of the user and cpu
		String scoreformat = "%-18s%3d\n";
		System.out.printf(scoreformat,"Player Score:", userScore);
		System.out.printf(scoreformat,"Computer Score:", cpuScore);
		System.out.println();

		//format and print the tiles of the user and cpu
		String tileformat = "%-38s%14s\n\n";
		System.out.printf(tileformat,"THE TILES IN YOUR HAND ARE:",userTiles);
		System.out.printf(tileformat,"THE TILES IN THE COMPUTER HAND ARE:",cpuTiles);

	}

	public void replaceTile(String player, int index)
	{
		//check if the game can end before moving on
		endGameIfPossible();
		int len = tilesRemaining.length();//number of tiles remaining
		char newTile = tilesRemaining.charAt((int)(Math.random()*(len-2))+1);//random tile from tilesremaining
		int bigIndex = tilesRemaining.indexOf(newTile);//index of the randomly selected tile

		//based on whose turn it is, replace the character they used with a new character
		//given the index of the character they used
		if(player.equals("User"))
		   {
			   userTiles = replaceChar(userTiles, index, newTile);
		   }

		else if(player.equals("Computer"))
		   {
			   cpuTiles = replaceChar(cpuTiles, index, newTile);
		   }
		//remove the tile given to the user from tilesremaining
		tilesRemaining = removeChar(tilesRemaining, bigIndex);
	}

	public String replaceChar(String str, int index, char newChar)
	{
		String newStr;//contains string with character replaced
		//concatenates the first part of the old string to the new character to the last part of the old string
		newStr = str.substring(0, index) + newChar + str.substring(index + 1);
		return newStr;
	}
	public String removeChar(String str, int index)
	{
		String newStr;//contains string with character removed
		//concatenates the first part of the old string to the last part without the character at the given index
		newStr = str.substring(0, index) + str.substring(index + 1);
		return newStr;
	}

	public void endGamePrint()
	{
		//print out message saying game ended and final scores of user and cpu
		System.out.println("\nNo more words can be created\n\n");
		System.out.printf("%-18s" + "%3d\n","Player Score:", userScore);
		System.out.printf("%-18s" + "%3d\n\n","Computer Score:", cpuScore);
		System.out.println("Thank you for playing ScrapplePlus!");
	}
	public void endGameIfPossible()
	{
		//if there are no more vowels end the game
		if(tilesRemaining.indexOf('A') == -1 &&
		   tilesRemaining.indexOf('E') == -1 &&
		   tilesRemaining.indexOf('I') == -1 &&
		   tilesRemaining.indexOf('O') == -1 &&
		   tilesRemaining.indexOf('U') == -1)
		{
			endGamePrint();
			System.exit(0);
		}
	}
}
