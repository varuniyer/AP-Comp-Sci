import java.util.Scanner;
import java.util.ArrayList;

public class SortMenu
{
	ArrayList <Integer> intList;

	public SortMenu ( )
	{
		intList = new ArrayList <Integer> ();
	}

	public static void main (String [] args)
	{
		SortMenu sortit = new SortMenu();
		sortit.run();
	}

	public void run ( )
	{
		char choice = '1';
		welcome();
		do
		{
			System.out.println("\n\n");
			int size = getSize();
			System.out.println("\n\n");
			int max = getMax();
			System.out.println("\n\n\n");
			createList(size, max);
			printList();
			chooseFromMenu("ArrayList of Integer");
			choice = repeatOrEnd();
		} while (choice == 'c' || choice == 'C');
		goodBye();
	}

	//  A welcome message.
	public void welcome ( )
	{
		System.out.println("\n\n\n\t\t\tAre you feeling OUT OF SORTS?\n\n");
		System.out.println("Welcome to the SORTING PROGRAM, a program that will take an Integer ArrayList");
		System.out.println("that is initially unordered, and then order the array using one of 3 quadratic");
		System.out.println("sorting algorithms, or an n log n sort.  These include a Bubble Sort, a Selection");
		System.out.println("Sort, an Insertion Sort, and a Merge Sort.  HAPPY SORTING!\n\n");
	}

	//  Gets the size of the array, from 5 to 10000.
	public int getSize ( )
	{
		int s = Prompt.getInt("Please enter the number of random integers you would like to generate ",5,10000);
		return s;
	}

	//  Gets the max value of the random values to be generated, from 1 to max.  The max should be from 5 to 10000.
	public int getMax ( )
	{
		int m = Prompt.getInt("Please enter the maximum value that you would like to possibly generate",5,10000);
		return m;
	}

	//  Generates an ArrayList of Integer values, with a size of s, and values from 1 to m.
	public void createList (int s, int m)
	{
		intList = new ArrayList<Integer>();
		for(int i = 0; i < s; i++)
		{
			intList.add((Integer)(int)(Math.random()*m + 1));
		}
	}

	//  Prints the ArrayList of Integer, printing a new line after every 15 values.
	public void printList ()
	{
		String spaceBetween = "  ";
		for(int i = 0; i < intList.size(); i++)
		{
			System.out.printf("%5d",intList.get(i));
			if((i+1)%15 == 0)
				System.out.println();
			else
				System.out.print(spaceBetween);
		}
		System.out.println("\n");
	}

	//  Calls menuOfSorts, getSortType, one of the sorts, then printList and printSteps.
	public void chooseFromMenu (String arraytype)
	{
		int steps;
		menuOfSorts(arraytype);
		char type = getSortType();
		System.out.println();
		switch(type)
		{
		case '1':
			steps = Sorts.bubbleSort(intList);
			break;
		case '2':
			steps = Sorts.selectionSort(intList);
			break;
		case '3':
			steps = Sorts.insertionSort(intList);
			break;
		case '4':
			steps = Sorts.mergeSort(intList, 0, intList.size() - 1);
			break;
		default:
			steps = 0;
		}
		System.out.println("\n\n");
		printList();
		printSteps(steps);
	}

	//  Prints the list of sort options.
	public void menuOfSorts (String title)
	{
		System.out.println("\n\n\n\n1. " + title + " Bubble Sort");
		System.out.println("2. " + title + " Selection Sort");
		System.out.println("3. " + title + " Insertion Sort");
		System.out.println("4. " + title + " Merge Sort\n\n");
	}

	//  Prompts the user to enter a character value from '1' to '4', indicating the sort
	//  to be run.
	public char getSortType ( )
	{
		char type = (char)(48 + Prompt.getInt("Enter a choice from the menu above ",1,4));
		return type;
	}

	//  Prints the number of steps taken by the sort.
	public void printSteps (int s)
	{
		System.out.println("\n\n" + s + " steps taken in this sort.\n\n");
	}

	//  Prompts the user to enter a 'c' or 'C' to continue, any other character to exit.
	public char repeatOrEnd ()
	{
		char c = (char)Prompt.getChar("Please enter a \'c\' or \'C\' to continue or any other character to exit");
		return c;
	}

	//  A goodbye message.
	public void goodBye ( )
	{
		System.out.println("\n\n\n\t\tThanks for working with the SORTING PROGRAM.");
		System.out.println("Hopefully, you were able to put your affairs in order(!).  Please run the program again");
		System.out.println("if you're interested in seeing quadratic sorting algorithms at work.\n\n\n\n");
	}
}
