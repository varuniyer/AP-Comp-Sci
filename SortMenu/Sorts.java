import java.util.ArrayList;

public class Sorts
{
	public static int bubbleSort (ArrayList <Integer> list)
	{
		int steps = 0;
		int temp;
		int index1 = 0;
		int index2 = 1;
		steps += 4;//for declaring/initializing above variables
		steps+=2;//for declaring/initializing i
		for(int i = list.size() - 1; i > 0; i--)
		{
			steps++;//for comparing i to 0
			index1 = 0;
			index2 = 1;
			steps += 2;//for reinitializing index1 and index2
			steps += 2;//for declaring and initializing j to 0
			for(int j = 0; j < i; j++)
			{
				steps++;//for comparing j to i each loop
				if(list.get(index1) > list.get(index2))
				{
					temp = list.get(index2);
					list.set(index2, list.get(index1));
					list.set(index1, temp);
					steps += 5; //for 5 total operations/comparisons inside if statement
				}
				index1++;
				index2++;
				steps += 2;//for these two increments
				steps += 3;//for making the if comparison and calling get methods
				steps++;//for incrementing j
			}
			steps++;//for comparing j to i to exit above loop
			steps++;//for decrementing i
		}
		steps++;//for comparing i to 0 to exit above loop
		return steps;
	}

	public static int selectionSort (ArrayList <Integer> list)
	{
		int steps = 0;
		int temp;
		int max = -1;
		int maxindex = 0;
		steps += 5; //for declaring/initializing above vars
		steps++; //for declaring/initializing i
		for(int i = list.size(); i > 0; i--)
		{
			steps++;//for comparing i to 0
			max = -1;
			steps += 2;//for initializing max and declaring j
			for(int j = 0; j < i; j++)
			{
				steps++;//for comparing j to i
				steps += 2;//for calling get method and comparing to max
				if(list.get(j) > max)
				{
					max = list.get(j);
					maxindex = j;
					steps += 3;//for lines inside if statement
				}
			}
			temp = list.get(i - 1);
			list.set(maxindex, temp);
			list.set(i - 1, max);
			steps += 4;//for 3 previous lines
			steps++;//for decrementing i
		}
		return steps;
	}

	public static int insertionSort (ArrayList <Integer> list)
	{
		int steps = 0;
		int temp;
		steps += 3;//for declaring temp and declaring/initializing i
		for (int i = 1; i < list.size(); i++) {
			temp = list.get(i);
			int j = i;
			steps += 3;//for initializing above two variables
			steps += 3;//for comparing in while
			while (j > 0 && list.get(j - 1) > temp)
			{
				list.set(j, list.get(j - 1));
				j--;
				steps += 3;//for lines inside while
			}
			list.set(j, temp);
			steps++;//for setting list at j
		}
		return steps;
	}

	private static ArrayList<Integer> temp;
	private static boolean init = false;
	private static int steps;
	public static int mergeSort (ArrayList<Integer> a, int from, int to)
	{
		if(!init)
		{
			steps = 0;
			int n = a.size();
			temp = new ArrayList<Integer>();
			for(int i = 0; i <= n; i++)
				temp.add(0);
			init = true;
		}

		if(to - from < 2)
		{
			if(to > from && a.get(to) < a.get(from))
			{
				Integer aTemp = a.get(to);
				a.set(to, a.get(from));
				a.set(from, aTemp);
			}
		}
		else
		{
			int middle = (from + to) / 2;
			mergeSort(a, from, middle);
			mergeSort(a, middle + 1, to);
			merge(a, from, middle, to);
		}
		return steps;
	}

	public static int merge (ArrayList<Integer> a, int from, int middle, int to)
	{
		int i = from;
		int j = middle + 1;
		int k = from;

		while(i <= middle && j <= to)
		{
			if(a.get(i) < a.get(j))
			{
				temp.set(k, a.get(i));
				i++;
			}
			else
			{
				temp.set(k, a.get(j));
				j++;
			}
			k++;
		}

		while(i <= middle)
		{
			temp.set(k, a.get(i));
			i++;
			k++;
		}

		while(j <= to)
		{
			temp.set(k, a.get(j));
			j++;
			k++;
		}

		for(k = from; k <= to; k++)
		{
			a.set(k, temp.get(k));
		}
		return steps;
	}
}
