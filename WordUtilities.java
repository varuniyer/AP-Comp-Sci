import java.util.Scanner;

/**
 *  Finds all the words that can be formed with a list of letters,
 *  then finds the word with the highest Scrabble score.
 *
 *  @author Varun Iyer
 *  @version 9/14/15
 */

public class WordUtilities
{
	private static Scanner infile;
	private static String infileName = "wordlist.txt";

	public WordUtilities() {
		String letters = "";
	}
	public static void main(String []args)
	{
		String input = getInput();
		String[] words = findAllWords(input);
		printWords(words);

		// Score table in alphabetic order according to Scrabble
		int [] scoretable = {1,3,3,2,1,4,2,4,1,8,5,1,3,1,1,3,10,1,1,1,1,4,4,8,4,10};
		String best = bestWord(words,scoretable);
		System.out.println("\n\n\n" + "Highest Scoring Word: " + best + "\n\n\n");
	}

	/**
	 *  Enter a list of 3 to 12 letters. It also checks
	 *  all letters to insure they fall between 'a' and 'z'.
	 *
	 *  @return  A string of the letters
	 */
	public static String getInput ( )
	{
		String l = "";

		while((l.length() < 4 || l.length() > 8))
		{
			l = Prompt.getString("Enter a word of 4 to 8 letters");
		}
		return l;
	}

	/**
	 *  Find all words that can be formed by a list of letters.
	 *
	 *  @param letters  String list of letters
	 *  @return words  An array of strings with all words found.
	 */
	public static String[] findAllWords (String letters)
	{
		String word;
		String[] words = new String[1000];
		int ind = 0;
		//Open the database file
		infile = OpenFile.openToRead(infileName);
		while(infile.hasNext()){
			word = infile.next();

			if(matchletters(word,letters))
			{
				words[ind] = word;
				ind++;
			}
		}
		return words;
	}

	/**
	 *  Checks if inputted string is a real word
	 *
	 *  @param letters  String list of letters
	 *  @return isWord  Whether or not it was a word
	 */
	public static boolean isWord (String letters)
	{
		String word;
		boolean isWord = false;
		int ind = 0;
		//Open the database file
		infile = OpenFile.openToRead(infileName);
		while(infile.hasNext()){
			word = infile.next();

			if(word.equals(letters))
			{
				isWord = true;
			}
		}
		return isWord;
	}


	/**
	 *  Determines if a word can be formed by a list of letters.
	 *
	 *  @param temp  The word to be tested.
	 *  @param letters  A string of the list of letter
	 *  @return True if word can be formed, false otherwise
	 */
	public static boolean matchletters (String temp, String letters)
	{
		for(int i = 0; i < temp.length(); i++)
		{
			int ind = letters.indexOf(temp.charAt(i));
			if(ind == -1)
				return false;
			letters = letters.substring(0,ind) +
			letters.substring(ind + 1);
		}
		return true;
	}

	/**
	 *  Print the words found to the screen.
	 *
	 *  @param word  The string array containing the words.
	 */
	public static void printWords (String [] word)
	{
		System.out.println();
		for(int i = 0; i < word.length; i++)
		{
			if(word[i]!=null)
			{
				System.out.printf("%s\t",word[i]);
				if((i+1) %5 == 0)
					System.out.println();
			}
		}
		System.out.println("\n");
	}

	/**
	 *  Finds the highest scoring word according to Scrabble rules.
	 *
	 *  @param word  An array of words to check.
	 *  @param scoretable  An array of 26 integer scores in letter order.
	 *  @return   The word with the highest score.
	 */
	public static String bestWord (String [] word, int [] scoretable)
	{
		int score = 0;
		int highscore = 0;
		String currentword = "";
		String bestword = "";
		for(int i = 0; i < word.length; i++)
		{
			currentword = word[i];
			if(word[i]!=null)
				score = getScore(currentword,scoretable,false," ");
			if(score > highscore)
			{
				highscore = score;
				bestword = currentword;
			}
		}
		return bestword;
	}

	/**
	 *  Calculates the score of a word according to Scrabble rules.
	 *
	 *  @param word  The word to score
	 *  @param scoretable  The array of 26 scores for alphabet.
	 *  @return   The integer score of the word.
	 */
	public static int getScore (String word, int [] scoretable, boolean print, String prevWord)
	{
		int score = 0;
		boolean doubles = false;
		for(int i = 0; i < word.length(); i++)
		{
			score+=scoretable[(int)(word.charAt(i))-97];
			if(i!=0)
				if(word.charAt(i)==word.charAt(i-1))
					doubles = true;
		}
		//check for potential bonus score
		if(doubles)
		{
			score *= 2;
			if(print)
				System.out.println("BONUS WORD SCORE!!!");
		}
		if((int)word.charAt(0) == 1 + (int)prevWord.charAt(0))
		{
			score *= 2;
			if(print)
				System.out.println("BONUS WORD SCORE!!!");
		}
		return score;
	}
}
