import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.text.*;
import java.util.*;
import java.util.List; // resolves problem with java.awt.List and java.util.List

/**
 * A class that represents a picture.  This class inherits from 
 * SimplePicture and allows the student to add functionality to
 * the Picture class.  
 * 
 * @author Barbara Ericson ericson@cc.gatech.edu
 */
public class Picture extends SimplePicture 
{
	///////////////////// constructors //////////////////////////////////
  
	/**
	 * Constructor that takes no arguments
	 */
	public Picture ()
	{
		/* not needed but use it to show students the implicit call to super()
		 * child constructors always call a parent constructor
		 */
		super();
	}
  
	/**
	 * Constructor that takes a file name and creates the picture
	 * @param fileName the name of the file to create the picture from
	 */
	public Picture(String fileName)
	{
		// let the parent class handle this fileName
		super(fileName);
	}
  
	/**
	 * Constructor that takes the width and height
	 * @param height the height of the desired picture
	 * @param width the width of the desired picture
	 */
	public Picture(int height, int width)
	{
		// let the parent class handle this width and height
		super(width,height);
	}
  
	/**
	 * Constructor that takes a picture and creates a
	 * copy of that picture
	 * @param copyPicture the picture to copy
	 */
	public Picture(Picture copyPicture)
	{
		// let the parent class do the copy
		super(copyPicture);
	}
  
	/**
	 * Constructor that takes a buffered image
	 * @param image the buffered image to use
	 */
	public Picture(BufferedImage image)
	{
		super(image);
	}
  
	////////////////////// methods ///////////////////////////////////////
  
	/**
	 * Method to return a string with information about this picture.
	 * @return a string with information about the picture such as fileName,
	 * height and width.
	 */
	public String toString()
	{
		String output = "Picture, filename " + getFileName() +
		                " height " + getHeight()
		                + " width " + getWidth();
		return output;
    
	}
  
	/** Method to set the blue to 0 */
	public void zeroBlue()
	{
		Pixel[][] pixels = this.getPixels2D();
		for (Pixel[] rowArray : pixels)
		{
			for (Pixel pixelObj : rowArray)
			{
				pixelObj.setBlue(0);
			}
		}
	}

	/** removes all colors except blue */
	public void keepOnlyBlue()
	{
		Pixel[][] pixels = this.getPixels2D();
		for (Pixel[] rowArray : pixels)
		{
			for (Pixel pixelObj : rowArray)
			{
				pixelObj.setRed(0);
				pixelObj.setGreen(0);
			}
		}
	}

	/** negates image colors */
	public void negate()
	{
		Pixel[][] pixels = this.getPixels2D();
		for (Pixel[] rowArray : pixels)
		{
			for (Pixel pixelObj : rowArray)
			{
				pixelObj.setRed(255 - pixelObj.getRed());
				pixelObj.setGreen(255 - pixelObj.getGreen());
				pixelObj.setBlue(255 - pixelObj.getBlue());
			}
		}
	}

	/** grays image */
	public void grayScale()
	{
		Pixel[][] pixels = this.getPixels2D();
		for (Pixel[] rowArray : pixels)
		{
			for (Pixel pixelObj : rowArray)
			{
				int avg = (int)((pixelObj.getRed() +
				                 pixelObj.getBlue() +
				                 pixelObj.getGreen())/3.0);
				pixelObj.setRed(avg);
				pixelObj.setGreen(avg);
				pixelObj.setBlue(avg);
			}
		}
	}

	/** Method that mirrors the picture around a
	 * vertical mirror in the center of the picture
	 * from left to right */
	public void mirrorVertical()
	{
		Pixel[][] pixels = this.getPixels2D();
		Pixel leftPixel = null;
		Pixel rightPixel = null;
		int width = pixels[0].length;
		for (int row = 0; row < pixels.length; row++)
		{
			for (int col = 0; col < width / 2; col++)
			{
				leftPixel = pixels[row][col];
				rightPixel = pixels[row][width - 1 - col];
				rightPixel.setColor(leftPixel.getColor());
			}
		}
	}

	public Picture swapLeftRight()
	{
		Pixel[][] pixels = this.getPixels2D();
		Picture result = new Picture(pixels.length, pixels[0].length);
		Pixel[][] resultPixels = result.getPixels2D();
	  
		int height = pixels.length;
		int width = pixels[0].length;
	  
		for(int row = 0; row < height; row++)
		{
			for(int col = 0; col < width; col++)
			{
				int newCol = (col + width / 2) % width;
				resultPixels[row][newCol].setRed(pixels[row][col].getRed());
				resultPixels[row][newCol].setGreen(pixels[row][col].getGreen());
				resultPixels[row][newCol].setBlue(pixels[row][col].getBlue());
			}
		}
		return result;
	}
  
	/** Mirror just part of a picture of a temple */
	public void mirrorTemple()
	{
		int mirrorPoint = 276;
		Pixel leftPixel = null;
		Pixel rightPixel = null;
		int count = 0;
		Pixel[][] pixels = this.getPixels2D();
    
		// loop through the rows
		for (int row = 27; row < 97; row++)
		{
			// loop from 13 to just before the mirror point
			for (int col = 13; col < mirrorPoint; col++)
			{
        
				leftPixel = pixels[row][col];
				rightPixel = pixels[row]
				             [mirrorPoint - col + mirrorPoint];
				rightPixel.setColor(leftPixel.getColor());
			}
		}
	}
  
	/** copy from the passed fromPic to the
	 * specified startRow and startCol in the
	 * current picture
	 * @param fromPic the picture to copy from
	 * @param startRow the start row to copy to
	 * @param startCol the start col to copy to
	 */
	public void copy(Picture fromPic,
	                 int startRow, int startCol)
	{
		Pixel fromPixel = null;
		Pixel toPixel = null;
		Pixel[][] toPixels = this.getPixels2D();
		Pixel[][] fromPixels = fromPic.getPixels2D();
		for (int fromRow = 0, toRow = startRow;
		     fromRow < fromPixels.length &&
		                   toRow < toPixels.length;
		     fromRow++, toRow++)
		{
			for (int fromCol = 0, toCol = startCol;
			     fromCol < fromPixels[0].length &&
			                   toCol < toPixels[0].length;
			     fromCol++, toCol++)
			{
				fromPixel = fromPixels[fromRow][fromCol];
				toPixel = toPixels[toRow][toCol];
				toPixel.setColor(fromPixel.getColor());
			}
		}
	}

	/** Method to create a collage of several pictures */
	public void createCollage()
	{
		Picture flower1 = new Picture("flower1.jpg");
		Picture flower2 = new Picture("flower2.jpg");
		this.copy(flower1,0,0);
		this.copy(flower2,100,0);
		this.copy(flower1,200,0);
		Picture flowerNoBlue = new Picture(flower2);
		flowerNoBlue.zeroBlue();
		this.copy(flowerNoBlue,300,0);
		this.copy(flower1,400,0);
		this.copy(flower2,500,0);
		this.mirrorVertical();
		this.write("collage.jpg");
	}
  
  
	/** Method to show large changes in color
	 * @param edgeDist the distance for finding edges
	 */
	public void edgeDetection(int edgeDist)
	{
		Pixel leftPixel = null;
		Pixel rightPixel = null;
		Pixel bottomPixel = null;
		Pixel[][] pixels = this.getPixels2D();
		Color rightColor = null;
		Color bottomColor = null;
		for (int row = 0; row < pixels.length - 1; row++)
		{
			for (int col = 0;
			     col < pixels[0].length-1; col++)
			{
				leftPixel = pixels[row][col];
				rightPixel = pixels[row][col+1];
				bottomPixel = pixels[row + 1][col];
				rightColor = rightPixel.getColor();
				bottomColor = bottomPixel.getColor();
				if (leftPixel.colorDistance(rightColor) >
				    edgeDist || leftPixel.colorDistance(bottomColor) >
				    edgeDist)
					leftPixel.setColor(Color.BLACK);
				else
					leftPixel.setColor(Color.WHITE);
			}
		}
	}
  
	/**
	 * makes stairsteps on pic
	 * @param shiftCount  separation between steps
	 * @param steps  amount of steps
	 */
	public Picture stairStep(int shiftCount, int steps)
	{
		Pixel[][] pixels = this.getPixels2D();
		Picture result = new Picture(pixels.length, pixels[0].length);
		Pixel[][] resultPixels = result.getPixels2D();
	  
		int height = pixels.length;
		int width = pixels[0].length;
		int stepHeight = height/steps;
	  
		for(int row = 0; row < height; row++)
		{
			int shift = shiftCount*(row/stepHeight);
			for(int col = 0; col < width; col++)
			{
				int newCol = (col + shift) % (width);
				resultPixels[row][newCol].setRed(pixels[row][col].getRed());
				resultPixels[row][newCol].setGreen(pixels[row][col].getGreen());
				resultPixels[row][newCol].setBlue(pixels[row][col].getBlue());
			}
		}
		return result;
	}
  
	/**
	 * adds gaussian curve effect to picture
	 * @param maxHeight  height of curve
	 * @param bellWidth  Width of curve
	 */
	public Picture liquify(int maxHeight, int bellWidth)
	{
		Pixel[][] pixels = this.getPixels2D();
		Picture result = new Picture(pixels.length, pixels[0].length);
		Pixel[][] resultPixels = result.getPixels2D();
	  
		int height = pixels.length;
		int width = pixels[0].length;
	  
		for(int row = 0; row < height; row++)
		{
			for(int col = 0; col < width; col++)
			{
				double exponent = Math.pow(row - height / 2.0, 2) / (2.0 * Math.pow(bellWidth, 2));
				int shift = (int)(maxHeight * Math.exp(- exponent));

				int newCol = (col + shift) % (width);
				resultPixels[row][newCol].setRed(pixels[row][col].getRed());
				resultPixels[row][newCol].setGreen(pixels[row][col].getGreen());
				resultPixels[row][newCol].setBlue(pixels[row][col].getBlue());
			}
		}
		return result;
	}
  
	/**
	 * adds sine wave effect to pic
	 * @param frequency  frequency of wave
	 * @param amplitude  amplitude of wave
	 * @param phaseShift  shift of wave
	 */
	public Picture waves(double frequency, int amplitude, int phaseShift)
	{
		Pixel[][] pixels = this.getPixels2D();
		Picture result = new Picture(pixels.length, pixels[0].length);
		Pixel[][] resultPixels = result.getPixels2D();
	  
		int height = pixels.length;
		int width = pixels[0].length;
	  
		for(int row = 0; row < height; row++)
		{
			for(int col = 0; col < width; col++)
			{
				int shift = (int)( amplitude * Math.sin(2 * Math.PI * frequency * row + phaseShift) );

				int newCol = (col + shift) % (width);
				if(newCol < 0)
				{
					newCol += width;
				}
				resultPixels[row][newCol].setRed(pixels[row][col].getRed());
				resultPixels[row][newCol].setGreen(pixels[row][col].getGreen());
				resultPixels[row][newCol].setBlue(pixels[row][col].getBlue());
			}
		}
		return result;
	}
  
	public void mirrorVerticalRightToLeft() {
		Pixel[][] pixels = this.getPixels2D();
		Pixel leftPixel = null;
		Pixel rightPixel = null;
		int width = pixels[0].length;
		for (int row = 0; row < pixels.length; row++) {
			for (int col = 0; col < width / 2; col++) {
				leftPixel = pixels[row][col];
				rightPixel = pixels[row][width - 1 - col];
				leftPixel.setColor(rightPixel.getColor());
			}
		}
	}

	public void mirrorHorizontal() {
		Pixel[][] pixels = this.getPixels2D();
		Pixel bottomPixel = null;
		Pixel topPixel = null;
		int width = pixels[0].length;
		for (int row = 0; row < pixels.length / 2; row++) {
			for (int col = 0; col < width; col++) {
				topPixel = pixels[row][col];
				bottomPixel = pixels[pixels.length - 1 - row][col];
				bottomPixel.setColor(topPixel.getColor());
			}
		}
	}
	
	public void mirrorHorizontalBotToTop() {
		Pixel[][] pixels = this.getPixels2D();
		Pixel bottomPixel = null;
		Pixel topPixel = null;
		int width = pixels[0].length;
		for (int row = 0; row < pixels.length / 2; row++) {
			for (int col = 0; col < width; col++) {
				topPixel = pixels[row][col];
				bottomPixel = pixels[pixels.length - 1 - row][col];
				topPixel.setColor(bottomPixel.getColor());
			}
		}
	}
	
	public void mirrorGull() {
		Pixel[][] pixels = this.getPixels2D();
		Pixel leftPixel = null;
		Pixel rightPixel = null;
		int width = pixels[0].length;
		for (int row = 0; row < pixels.length; row++)
		{
			for (int col = 0; col < width / 2 + 50; col++)
			{
				leftPixel = pixels[row][col];
				rightPixel = pixels[row][(width + 50 - col)%width];
				rightPixel.setColor(leftPixel.getColor());
			}
		}
	}

	public void mirrorDiagonal()
	{
		Pixel[][] pixels = this.getPixels2D();
		Pixel leftPixel = null;
		Pixel rightPixel = null;
		int squareLen;
		if(pixels[0].length > pixels.length)
			squareLen = pixels.length;
		else
			squareLen = pixels[0].length;
		for (int row = 0; row < squareLen; row++)
		{
			for (int col = 0; col < row; col++)
			{
				leftPixel = pixels[row][col];
				rightPixel = pixels[col][row];
				rightPixel.setColor(leftPixel.getColor());
			}
		}
	}

	/* Main method for testing - each class in Java can have a main
	 * method
	 */
	public static void main(String[] args)
	{
		Picture beach = new Picture("images/beach.jpg");
		beach.explore();
		beach.zeroBlue();
		beach.explore();
	}
  
} // this } is the end of class Picture, put all new methods before this
