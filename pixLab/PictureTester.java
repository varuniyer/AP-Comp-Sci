/**
 * This class contains class (static) methods
 * that will help you test the Picture class
 * methods.  Uncomment the methods and the code
 * in the main to test.
 *
 * @author Barbara Ericson
 */
public class PictureTester
{
  /** Method to test zeroBlue */
  public static void testZeroBlue()
  {
    Picture pic = new Picture("images/Orbs.png").scale(2,2);
    pic.explore();
    pic.zeroBlue();
    pic.explore();
  }

  /** method to test keepOnlyBlue */
  public static void testKeepOnlyBlue()
  {
    Picture pic = new Picture("images/Orbs.png").scale(2,2);
    pic.explore();
    pic.keepOnlyBlue();
    pic.explore();
  }

  /** Method to test negate */
  public static void testNegate()
  {
    Picture pic = new Picture("images/Orbs.png").scale(2,2);
    pic.explore();
    pic.negate();
    pic.explore();
  }

  /** Method to test grayScale */
  public static void testGrayScale()
  {
    Picture pic = new Picture("images/Orbs.png").scale(2,2);
    pic.explore();
    pic.grayScale();
    pic.explore();
  }
  
  /** Method to test mirrorVerticalRightToLeft */
  public static void testMirrorVerticalRightToLeft()
  {
    Picture pic = new Picture("images/Orbs.png").scale(2,2);
    pic.explore();
    pic.mirrorVerticalRightToLeft();
    pic.explore();
  }

  /** Method to test mirrorVertical */
  public static void testMirrorVertical()
  {
    Picture caterpillar = new Picture("images/Orbs.png").scale(2,2);
    caterpillar.explore();
    caterpillar.mirrorVertical();
    caterpillar.explore();
  }

  /** Method to test mirrorTemple */
  public static void testMirrorTemple()
  {
    Picture temple = new Picture("images/Orbs.png").scale(2,2);
    temple.explore();
    temple.mirrorTemple();
    temple.explore();
  }

  /** Method to test the collage method */
  public static void testCollage()
  {
    Picture canvas = new Picture("images/Orbs.png").scale(2,2);
    canvas.createCollage();
    canvas.explore();
  }

  /** Method to test edgeDetection */
  public static void testEdgeDetection()
  {
    Picture swan = new Picture("images/swan.jpg");
    swan.explore();
    swan.edgeDetection(10);
    swan.explore();
  }
  
  /** Method to test swapLeftRight */
  public static void testSwapLeftRight()
  {
	  Picture orbs = new Picture("images/Orbs.png").scale(2,2);
	  orbs.explore();
	  orbs = orbs.swapLeftRight();
	  orbs.explore();
  }
  
  public static void testStairStep()
  {
	  Picture orbs = new Picture("images/Orbs.png").scale(2,2);
	  orbs.explore();
	  Picture newOrbs = orbs.stairStep(1,400);
	  newOrbs.explore();
  }
  
  public static void testLiquify()
  {
	  Picture orbs = new Picture("images/Orbs.png").scale(2,2);
	  orbs.explore();
	  Picture newOrbs = orbs.liquify(100,100);
	  newOrbs.explore();
  }
  
  public static void testWaves()
  {
	  Picture orbs = new Picture("images/Orbs.png").scale(2,2);
	  orbs.explore();
	  Picture newOrbs = orbs.waves(0.005,100,100);
	  newOrbs.explore();
  }
  
  public static void testMirrorHorizontal()
  {
	  Picture orbs = new Picture("images/Orbs.png").scale(2,2);
	  orbs.explore();
	  orbs.mirrorHorizontal();
	  orbs.explore();
  }
  
  public static void testMirrorHorizontalBotToTop()
  {
	  Picture orbs = new Picture("images/Orbs.png").scale(2,2);
	  orbs.explore();
	  orbs.mirrorHorizontalBotToTop();
	  orbs.explore();
  }
  
  public static void testMirrorGull()
  {
    Picture orbs = new Picture("seagull.jpg");
    orbs.explore();
    orbs.mirrorGull();
    orbs.explore();
  }

  public static void testMirrorDiagonal()
  {
    Picture orbs = new Picture("images/Orbs.png").scale(2,2);
    orbs.explore();
    orbs.mirrorDiagonal();
    orbs.explore();
  }

  /** Main method for testing.  Every class can have a main
   * method in Java */
  public static void main(String[] args)
  {
    // uncomment a call here to run a test
    // and comment out the ones you don't want
    // to run
    //testMirrorDiagonal();
      //testMirrorGull();
    //testMirrorHorizontalBotToTop();
    //testMirrorHorizontal();
    //testWaves();
    //testLiquify();
    //testStairStep();
    //testSwapLeftRight();
    //testZeroBlue();
    //testKeepOnlyBlue();
    //testKeepOnlyRed();
    //testKeepOnlyGreen();
    //testNegate();
    //testGrayScale();
    //testFixUnderwater();
    //testMirrorVertical();
    //testMirrorVerticalRightToLeft();
    //testMirrorTemple();
    //testMirrorArms();
    //testMirrorGull();
    //testMirrorDiagonal();
    //testCollage();
    //testCopy();
    testEdgeDetection();
    //testEdgeDetection2();
    //testChromakey();
    //testEncodeAndDecode();
    //testGetCountRedOverValue(250);
    //testSetRedToHalfValueInTopHalf();
    //testClearBlueOverValue(200);
    //testGetAverageForColumn(0);
  }
}
